<?php

declare(strict_types=1);

namespace App\Tests;

use PHPUnit\Framework\TestCase;

class AppTest extends TestCase
{
    public function testApp(): void
    {
        $files = [];

        $i = 0;

        try {
            while (true) {
                $tmpFile = tmpfile();

                if (false === $tmpFile) {
                    echo 'Unable to create temp file, File count: '.count($files).PHP_EOL;
                    break;
                }

                $files[] = tmpfile();

                if (1 === ($i / 10000)) {
                    $i = 1;
                    echo 'File count: '.count($files).PHP_EOL;
                }
            }
        } catch (\Throwable $exception) {
            echo $exception->getMessage().PHP_EOL;
        }

        echo 'File count: '.count($files).PHP_EOL;
    }
}
